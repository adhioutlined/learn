Minikube is a tool that makes it easy to run Kubernetes locally. Minikube runs a single-node Kubernetes cluster inside a VM on your laptop for users looking to try out Kubernetes or develop with it day-to-day.

More details can be found at  [https://github.com/kubernetes/minikube](https://github.com/kubernetes/minikube)

#### Step 1 - Start Minikube

Minikube has been installed and configured in the environment. Check that it is properly installed, by running the  _minikube version_  command:

`minikube version`

Start the cluster, by running the  _minikube start_  command:

`minikube start --wait=false`

Great! You now have a running Kubernetes cluster in your online terminal. Minikube started a virtual machine for you, and a Kubernetes cluster is now running in that VM.

#### Step 2 - Cluster Info

The cluster can be interacted with using the  _kubectl_  CLI. This is the main approach used for managing Kubernetes and the applications running on top of the cluster.

Details of the cluster and its health status can be discovered via  `kubectl cluster-info`

To view the nodes in the cluster using  `kubectl get nodes`

If the node is marked as  **NotReady**  then it is still starting the components.

This command shows all nodes that can be used to host our applications. Now we have only one node, and we can see that it’s status is ready (it is ready to accept applications for deployment).

#### Step 3 - Deploy Containers

With a running Kubernetes cluster, containers can now be deployed.

Using  `kubectl run`, it allows containers to be deployed onto the cluster -  `kubectl create deployment first-deployment --image=katacoda/docker-http-server`

The status of the deployment can be discovered via the running Pods -  `kubectl get pods`

Once the container is running it can be exposed via different networking options, depending on requirements. One possible solution is NodePort, that provides a dynamic port to a container.

`kubectl expose deployment first-deployment --port=80 --type=NodePort`

The command below finds the allocated port and executes a HTTP request.

`export PORT=$(kubectl get svc first-deployment -o go-template='{{range.spec.ports}}{{if .nodePort}}{{.nodePort}}{{"\n"}}{{end}}{{end}}') echo "Accessing host01:$PORT" curl host01:$PORT`

The result is the container that processed the request.

#### Step 4 - Dashboard

Enable the dashboard using Minikube with the command  `minikube addons enable dashboard`

Make the Kubernetes Dashboard available by deploying the following YAML definition. This should only be used on Katacoda.

`kubectl apply -f /opt/kubernetes-dashboard.yaml`

The Kubernetes dashboard allows you to view your applications in a UI. In this deployment, the dashboard has been made available on port  _30000_  but may take a while to start.

To see the progress of the Dashboard starting, watch the Pods within the  _kube-system_  namespace using  `kubectl get pods -n kubernetes-dashboard -w`

Once running, the URL to the dashboard is  [https://167772196-30000-kitek05.environments.katacoda.com/](https://167772196-30000-kitek05.environments.katacoda.com/)

```
$ minikube version
minikube version: v1.8.1
commit: cbda04cf6bbe65e987ae52bb393c10099ab62014
$ minikube start --wait=false
* minikube v1.8.1 on Ubuntu 18.04
* Using the none driver based on user configuration
* Running on localhost (CPUs=2, Memory=2460MB, Disk=145651MB) ...
* OS release is Ubuntu 18.04.4 LTS
* Preparing Kubernetes v1.17.3 on Docker 19.03.6 ...
  - kubelet.resolv-conf=/run/systemd/resolve/resolv.conf
* Launching Kubernetes ... 
* Enabling addons: default-storageclass, storage-provisioner
* Configuring local host environment ...
* Done! kubectl is now configured to use "minikube"
$ 
$ kubectl cluster-info
Kubernetes master is running at https://10.0.0.36:8443
KubeDNS is running at https://10.0.0.36:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
$ kubectl get nodes
NAME       STATUS   ROLES    AGE   VERSION
minikube   Ready    master   58s   v1.17.3
$ 
$ kubectl create deployment first-deployment --image=katacoda/docker-http-server
deployment.apps/first-deployment created
$ kubectl get pods
NAME                               READY   STATUS    RESTARTS   AGE
first-deployment-666c48b44-9cxk8   1/1     Running   0          4s
$ kubectl expose deployment first-deployment --port=80 --type=NodePort
service/first-deployment exposed
$ export PORT=$(kubectl get svc first-deployment -o go-template='{{range.spec.ports}}{{if .nodePort}}{{.nodePort}}{{"\n"}}{{end}}{{end}}')$ echo "Accessing host01:$PORT"
Accessing host01:32273
$ curl host01:$PORT
<h1>This request was processed by host: first-deployment-666c48b44-9cxk8</h1>
$ 
$ minikube addons enable dashboard
* The 'dashboard' addon is enabled
$ kubectl apply -f /opt/kubernetes-dashboard.yaml
namespace/kubernetes-dashboard configured
service/kubernetes-dashboard-katacoda created
$ kubectl get pods -n kubernetes-dashboard -w
NAME                                         READY   STATUS    RESTARTS   AGE
dashboard-metrics-scraper-7b64584c5c-pd7q8   1/1     Running   0          6s
kubernetes-dashboard-79d9cd965-zxls5         1/1     Running   0          6s

```