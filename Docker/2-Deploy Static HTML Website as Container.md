In this scenario, you'll learn how to create a Docker Image for running a static HTML website using Nginx. The scenario will explain how to build a Docker Image running Nginx with your HTML site.

The aim is to help you understand how to create and run Docker Images created by yourself.

#### Step 1 - Create Dockerfile

Docker Images start from a base image. The base image should include the platform dependencies required by your application, for example, having the JVM or CLR installed.

This base image is defined as an instruction in the Dockerfile. Docker Images are built based on the contents of a Dockerfile. The Dockerfile is a list of instructions describing how to deploy your application.

In this example, our base image is the Alpine version of Nginx. This provides the configured web server on the Linux Alpine distribution.

## Task

Create your  _Dockerfile_  for building your image by copying the contents below into the editor.

```
FROM nginx:alpine 
COPY . /usr/share/nginx/html 
```

The first line defines our base image. The second line copies the content of the current directory into a particular location inside the container.

#### Step 2 - Build Docker Image

The Dockerfile is used by the Docker CLI  _build_  command. The  _build_  command executes each instruction within the Dockerfile. The result is a built Docker Image that can be launched and run your configured app.

The build command takes in some different parameters. The format is  _docker build -t  <build-directory>_. The  _-t_  parameter allows you to specify a friendly name for the image and a tag, commonly used as a version number. This allows you to track built images and be confident about which version is being started.

## Task

Build our static HTML image using the build command below.

`docker build -t webserver-image:v1 .`

You can view a list of all the images on the host using  `docker images`.

The built image will have the name  _webserver-image_  with a tag of  _v1_.

#### Step 3 - Run

The built Image can be launched in a consistent way to other Docker Images. When a container launches, it's sandboxed from other processes and networks on the host. When starting a container you need to give it permission and access to what it requires.

For example, to open and bind to a network port on the host you need to provide the parameter  _-p <host-port>:<container-port>_.

## Task

Launch our newly built image providing the friendly name and tag. As it's a web server, bind port 80 to our host using the  _-p_  parameter.

`docker run -d -p 80:80 webserver-image:v1`

Once started, you'll be able to access the results of port 80 via  `curl docker`

To render the requests in the browser use the following links

[https://167772168-80-simba08.environments.katacoda.com/](https://167772168-80-simba08.environments.katacoda.com/)

#### Complete Command
```
Your Interactive Bash Terminal. A safe place to learn and execute commands.

$ docker build -t webserver-image:v1 .
Sending build context to Docker daemon  3.072kB
Step 1/2 : FROM nginx:alpine
 ---> b1c3acb28882
Step 2/2 : COPY . /usr/share/nginx/html
 ---> deeb1caad07a
Successfully built deeb1caad07a
Successfully tagged webserver-image:v1
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
webserver-image     v1                  deeb1caad07a        3 seconds ago       23.4MB
nginx               alpine              b1c3acb28882        17 hours ago        23.4MB
redis               latest              4760dc956b2d        4 years ago         107MB
ubuntu              latest              f975c5035748        4 years ago         112MB
alpine              latest              3fd9065eaf02        4 years ago         4.14MB
$ docker run -d -p 80:80 webserver-image:v1
cda81444682651dd1889476e8efb97ba9086949c367d73567203a24bfadd6708
$ curl docker
<h1>Hello World</h1>
$ ^C
$ 
```

You now have a static HTML website being served by Nginx.