When running containers in production, it can be useful to add additional metadata relating to the container to help their management. This metadata could be related to which version of the code is running, which applications or users own the container or define special criteria such as which servers they should run on.

This additional data is managed via Docker Labels. Labels allow you to define custom metadata about a container or image which can later be inspected or used as part of a filter.

This scenario guides you though creating and querying the metadata for containers and images.

#### Recommended Guidelines

There are recommended guidelines to follow when working with labels to ensure they're consistent and manageable as defined by  [Docker](https://www.katacoda.com/courses/docker/15).

Firstly, all tools should prefix their keys with the reverse DNS notation of a domain controlled by the author. For example, com.katacoda.some-label.

Secondly, if you're creating labels for CLI use, then they should follow the DNS notation making it easier for users to type.

Finally, keys should only consist of lower-cased alphanumeric characters, dots and dashes (for example, [a-z0-9-.])

#### Step 1 - Docker Containers

Labels can be attached to containers when they are launched via  `docker run`. A container can have multiple labels attached to them at any one time.

Notice in this example, because we're using the labels are for use with the CLI, and not an automated tool, we're not using the DNS notation format.

#### Single Label

To add a single label you use the  _l  =<value>_  option. The example below assigns a label called user with an ID to the container. This would allow us to query for all the containers running related to that particular user.

`docker run -l user=12345 -d redis`

#### External File

If you're adding multiple labels, then these can come from an external file. The file needs to have a label on each line, and then these will be attached to the running container.

This line creates two labels in the file, one for the user and the second assigning a role.

`echo 'user=123461' >> labels && echo 'role=cache' >> labels`

The  _--label-file=<filename>_  option will create a label for each line in the file.

`docker run --label-file=labels -d redis`

#### Step 2 - Docker Images

Labelling images work in the same way as containers but are set in the  _Dockerfile_  when the image is built. When a container has launched the labels of the image will be applied to the container instance.

#### Single Label

Within a  _Dockerfile_  you can assign a label using the LABEL instruction. Below the label  _vendor_  is created with the name Scrapbook.

LABEL vendor=Katacoda

#### Multiple Labels

If we want to assign multiple labels then, we can use the format below with a label on each line, joined using a back-slash ("\"). Notice we're using the DNS notation format for labels which are related to third party tooling.

LABEL vendor=Katacoda \ com.katacoda.version=0.0.5 \ com.katacoda.build-date=2016-07-01T10:47:29Z \ com.katacoda.course=Docker

#### Step 3 - Inspect

Labels and Metadata are only useful if you can view/query them later. The first approach to viewing all the labels for a particular container or image is by using  `docker inspect`.

#### Task

The environment has created a container, named rd, and an image, named katacoda-label-example, for you. Query their labels to find out additional information about them.

#### Container

By providing the running container's friendly name or hash id, you can query all of it's metadata.

`docker inspect rd`

Using the  _-f_  option you can filter the JSON response to just the Labels section we're interested in.

`docker inspect -f "{{json .Config.Labels }}" rd`

#### Image

Inspecting images works in the same way however the JSON format is slightly different, naming it  _ContainerConfig_  instead of  _Config_.

`docker inspect -f "{{json .ContainerConfig.Labels }}" katacoda-label-example`

These labels will remain even if the image has been untagged. When an image is untagged, it will have the name  _<none>_.

#### Step 5 - Daemon labels

Labels are not only applied to images and containers but also the Docker Daemon itself. When you launch an instance of the daemon, you can assign it labels to help identify how it should be used, for example, if it's a development or production server or if it's more suited to particular roles such running databases.

We'll explore more about customising Docker's configuration and how labels are used in future scenarios, but as a taster, the syntax is below.

```
docker -d \
  -H unix:///var/run/docker.sock \
  --label com.katacoda.environment="production" \
  --label com.katacoda.storage="ssd"
```

#### Complete Command
```
$ docker run -l user=12345 -d redis
259d63518380763cb3eb84274021dfc275caf7f1bcffa1e3b346ccec9e00dee2
$ echo 'user=123461' >> labels && echo 'role=cache' >> labels
$ docker run --label-file=labels -d redis
2a24b10bfb007962b0ebff043bbb225d298fec5c2ee20f3fc4faa88575b12b62
$ docker inspect rd
[
    {
        "Id": "f891902f21084b819d0a2f648c32b655ba0f8f39e57ebca66c6d9dce5d8b54b7",
        "Created": "2022-05-19T05:02:11.201892188Z",
        "Path": "docker-entrypoint.sh",
        "Args": [
            "redis-server"
        ],
        "State": {
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 539,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2022-05-19T05:02:11.586407785Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        },
        "Image": "sha256:4760dc956b2ddc9ac1c508936e39b63a22c6f0640ef58c1b10ff73f04e253ffe",
        "ResolvConfPath": "/var/lib/docker/containers/f891902f21084b819d0a2f648c32b655ba0f8f39e57ebca66c6d9dce5d8b54b7/resolv.conf",
        "HostnamePath": "/var/lib/docker/containers/f891902f21084b819d0a2f648c32b655ba0f8f39e57ebca66c6d9dce5d8b54b7/hostname",
        "HostsPath": "/var/lib/docker/containers/f891902f21084b819d0a2f648c32b655ba0f8f39e57ebca66c6d9dce5d8b54b7/hosts",
        "LogPath": "/var/lib/docker/containers/f891902f21084b819d0a2f648c32b655ba0f8f39e57ebca66c6d9dce5d8b54b7/f891902f21084b819d0a2f648c32b655ba0f8f39e57ebca66c6d9dce5d8b54b7-json.log",
        "Name": "/rd",
        "RestartCount": 0,
        "Driver": "overlay",
        "Platform": "linux",
        "MountLabel": "",
        "ProcessLabel": "",
        "AppArmorProfile": "",
        "ExecIDs": null,
        "HostConfig": {
            "Binds": null,
            "ContainerIDFile": "",
            "LogConfig": {
                "Type": "json-file",
                "Config": {}
            },
            "NetworkMode": "default",
            "PortBindings": {},
            "RestartPolicy": {
                "Name": "no",
                "MaximumRetryCount": 0
            },
            "AutoRemove": false,
            "VolumeDriver": "",
            "VolumesFrom": null,
            "CapAdd": null,
            "CapDrop": null,
            "Dns": [],
            "DnsOptions": [],
            "DnsSearch": [],
            "ExtraHosts": null,
            "GroupAdd": null,
            "IpcMode": "shareable",
            "Cgroup": "",
            "Links": null,
            "OomScoreAdj": 0,
            "PidMode": "",
            "Privileged": false,
            "PublishAllPorts": false,
            "ReadonlyRootfs": false,
            "SecurityOpt": null,
            "UTSMode": "",
            "UsernsMode": "",
            "ShmSize": 67108864,
            "Runtime": "runc",
            "ConsoleSize": [
                0,
                0
            ],
            "Isolation": "",
            "CpuShares": 0,
            "Memory": 0,
            "NanoCpus": 0,
            "CgroupParent": "",
            "BlkioWeight": 0,
            "BlkioWeightDevice": [],
            "BlkioDeviceReadBps": null,
            "BlkioDeviceWriteBps": null,
            "BlkioDeviceReadIOps": null,
            "BlkioDeviceWriteIOps": null,
            "CpuPeriod": 0,
            "CpuQuota": 0,
            "CpuRealtimePeriod": 0,
            "CpuRealtimeRuntime": 0,
            "CpusetCpus": "",
            "CpusetMems": "",
            "Devices": [],
            "DeviceCgroupRules": null,
            "DiskQuota": 0,
            "KernelMemory": 0,
            "MemoryReservation": 0,
            "MemorySwap": 0,
            "MemorySwappiness": null,
            "OomKillDisable": false,
            "PidsLimit": 0,
            "Ulimits": null,
            "CpuCount": 0,
            "CpuPercent": 0,
            "IOMaximumIOps": 0,
            "IOMaximumBandwidth": 0
        },
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay/d5d206b9df9a3642afb5a4767df56cf1770db74b9de93724098e86acf3ff97f3/root",
                "MergedDir": "/var/lib/docker/overlay/723d73374dad7d98432774724127b191351723cbaf6f311939b49f3669b659cb/merged",
                "UpperDir": "/var/lib/docker/overlay/723d73374dad7d98432774724127b191351723cbaf6f311939b49f3669b659cb/upper",
                "WorkDir": "/var/lib/docker/overlay/723d73374dad7d98432774724127b191351723cbaf6f311939b49f3669b659cb/work"
            },
            "Name": "overlay"
        },
        "Mounts": [
            {
                "Type": "volume",
                "Name": "8b1690f648b2a466d789e52cdb5f34596b441ff1425e76160a7962e5c43ebba3",
                "Source": "/var/lib/docker/volumes/8b1690f648b2a466d789e52cdb5f34596b441ff1425e76160a7962e5c43ebba3/_data",
                "Destination": "/data",
                "Driver": "local",
                "Mode": "",
                "RW": true,
                "Propagation": ""
            }
        ],
        "Config": {
            "Hostname": "f891902f2108",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "ExposedPorts": {
                "6379/tcp": {}
            },
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "GOSU_VERSION=1.10",
                "REDIS_VERSION=4.0.8",
                "REDIS_DOWNLOAD_URL=http://download.redis.io/releases/redis-4.0.8.tar.gz",
                "REDIS_DOWNLOAD_SHA=ff0c38b8c156319249fec61e5018cf5b5fe63a65b61690bec798f4c998c232ad"
            ],
            "Cmd": [
                "redis-server"
            ],
            "ArgsEscaped": true,
            "Image": "redis",
            "Volumes": {
                "/data": {}
            },
            "WorkingDir": "/data",
            "Entrypoint": [
                "docker-entrypoint.sh"
            ],
            "OnBuild": null,
            "Labels": {
                "com.katacoda.created": "automatically",
                "com.katacoda.private-msg": "magic",
                "user": "scrapbook"
            }
        },
        "NetworkSettings": {
            "Bridge": "",
            "SandboxID": "686d27bf36cd5c0faa6c56ecd41ec6b77707e95555f02dc4a6d26f1bd5c44006",
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "Ports": {
                "6379/tcp": null
            },
            "SandboxKey": "/var/run/docker/netns/686d27bf36cd",
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "EndpointID": "633af486439fd895be4b1098557ec4f043ca3321f73a00281c8d85d89dca323a",
            "Gateway": "172.18.0.1",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.18.0.2",
            "IPPrefixLen": 24,
            "IPv6Gateway": "",
            "MacAddress": "02:42:ac:12:00:02",
            "Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "06c848d84a4a597e47bbe109ba227c9157265a8c9879b7b35d4d8d5fa7135a13",
                    "EndpointID": "633af486439fd895be4b1098557ec4f043ca3321f73a00281c8d85d89dca323a",
                    "Gateway": "172.18.0.1",
                    "IPAddress": "172.18.0.2",
                    "IPPrefixLen": 24,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:12:00:02",
                    "DriverOpts": null
                }
            }
        }
    }
]
$ docker inspect -f "{{json .Config.Labels }}" rd
{"com.katacoda.created":"automatically","com.katacoda.private-msg":"magic","user":"scrapbook"}
$ docker inspect -f "{{json .ContainerConfig.Labels }}" katacoda-label-example
{"com.katacoda.build-date":"2015-07-01T10:47:29Z","com.katacoda.course":"Docker","com.katacoda.private-msg":"HelloWorld","com.katacoda.version":"0.0.5","vendor":"Katacoda"}
$
```