This scenario explores how to allow multiple containers to communicate with each other. The steps will explain how to connect a data-store, in this case, Redis, to an application running in a separate container.

This environment has been configured with a Docker client and daemon. The machine name the Docker daemon is running on is called  _docker_. If you want to access any of the services, then use  _docker_  instead of localhost or 0.0.0.0.

#### Step 1 - Start Redis

The most common scenario for connecting to containers is an application connecting to a data-store. The key aspect when creating a link is the name of the container. All containers have names, but to make it easier when working with links, it's important to define a friendly name of the source container which you're connecting to.

#### Start Data Store

Run a redis server with a friendly name of  _redis-server_  which we'll connect to in the next step. This will be our source container.

`docker run -d --name redis-server redis`

###### Redis

Redis is a fast, open source, key-value data store.

#### Step 2 - Create Link

To connect to a source container you use the  _--link <container-name|id>:<alias>_  option when launching a new container. The container name refers to the source container we defined in the previous step while the alias defines the friendly name of the host.

By setting an alias we separate how our application is configured to how the infrastructure is called. This means the application configuration doesn't need to change as it's connected to other environments.

#### How Links Work

In this example, we bring up a Alpine container which is linked to our  _redis-server_. We've defined the alias as  _redis_. When a link is created, Docker will do two things.

First, Docker will set some environment variables based on the linked to the container. These environment variables give you a way to reference information such as Ports and IP addresses via known names.

You can output all the environment variables with the  `env`  command. For example:

`docker run --link redis-server:redis alpine env`

Secondly, Docker will update the  _HOSTS_  file of the container with an entry for our source container with three names, the original, the alias and the hash-id. You can output the containers host entry using  `cat /etc/hosts`

`docker run --link redis-server:redis alpine cat /etc/hosts`

#### Example

With a link created you can ping the source container in the same way as if it were a server running in your network.

`docker run --link redis-server:redis alpine ping -c 1 redis`

#### Step 3 - Connect To App

With a link created, applications can connect and communicate with the source container in the usual way, independent of the fact both services are running in containers.

#### Example Application

Here is a simple node.js application which connects to redis using the hostname  _redis_.

`docker run -d -p 3000:3000 --link redis-server:redis katacoda/redis-node-docker-example`

#### Test Connection

Sending an HTTP request to the application will store the request in Redis and return a count. If you issue multiple requests, you'll see the counter increment as items are persisted.

`curl docker:3000`

#### Step 4 - Connect to Redis CLI

In the same way, you can connect source containers to applications, you can also connect them to their own CLI tools.

#### Launching Redis CLI

The command below will launch an instance of the Redis-cli tool and connect to the redis server via its alias.

`docker run -it --link redis-server:redis redis redis-cli -h redis`

#### Issuing Commands

The command  `KEYS *`  will output the contents stored currently in the source redis container.

Type  `QUIT`  to exit the CLI.

#### Complete Commands
```
$ docker run -d --name redis-server redis
dea20d53e8d53b6130b75798e74912dd3c6a73b307df2c23a952f823f4fe895e
$ docker run --link redis-server:redis alpine env
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=665e732ce10f
REDIS_PORT=tcp://172.18.0.2:6379
REDIS_PORT_6379_TCP=tcp://172.18.0.2:6379
REDIS_PORT_6379_TCP_ADDR=172.18.0.2
REDIS_PORT_6379_TCP_PORT=6379
REDIS_PORT_6379_TCP_PROTO=tcp
REDIS_NAME=/cranky_tereshkova/redis
REDIS_ENV_GOSU_VERSION=1.14
REDIS_ENV_REDIS_VERSION=7.0.0
REDIS_ENV_REDIS_DOWNLOAD_URL=http://download.redis.io/releases/redis-7.0.0.tar.gz
REDIS_ENV_REDIS_DOWNLOAD_SHA=284d8bd1fd85d6a55a05ee4e7c31c31977ad56cbf344ed83790beeb148baa720
HOME=/root
$ docker run --link redis-server:redis alpine cat /etc/hosts
127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.18.0.2      redis dea20d53e8d5 redis-server
172.18.0.3      64b94367cead
$ docker run --link redis-server:redis alpine ping -c 1 redis
PING redis (172.18.0.2): 56 data bytes
64 bytes from 172.18.0.2: seq=0 ttl=64 time=0.094 ms

--- redis ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 0.094/0.094/0.094 ms
$ ls
$ ls -alh
total 12K
drwxr-xr-x 2 scrapbook scrapbook 4.0K Apr 14  2019 .
drwxr-xr-x 1 scrapbook scrapbook 4.0K Apr 14  2019 ..
$ docker run -d -p 3000:3000 --link redis-server:redis katacoda/redis-node-docker-example
3e861a7a79ceace8f2990792094aa7c0dc4e162eef9e99b606db20eb1cbf3db8
$ curl docker:3000
This page was generated after talking to redis.

Application Build: 1

Total requests: 1

IP count: 
    ::ffff:10.0.0.11: 1
$ docker run -it --link redis-server:redis redis redis-cli -h redis
redis:6379> KEYS *
1) "ip"
2) "requests"
redis:6379> QUIT
$ 
```