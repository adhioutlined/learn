With Docker all the containers are managed via the Docker Daemon. The Daemon controls all aspects of the container lifecycle.

Previous versions of Docker required that the Daemon started by user with root privileges. This required giving users full access to a machine in order to control and configure Docker. As a result, this exposed potential security risks.

Rootless Docker is a project from Docker that removes the requirement for the Docker Daemon to be started by a root. This creates a more secure environment.

In this scenario, you will learn how to deploy Rootless Docker from a low privileged user, and how user will be able to manage and control the containers running on the system.

More information at  [https://engineering.docker.com/2019/02/experimenting-with-rootless-docker/](https://engineering.docker.com/2019/02/experimenting-with-rootless-docker/)

#### Step 1 - Create Ubuntu User

The environment is currently running Ubuntu 16.04 with the user logged in as root. The first step is to create a new user without these root privileges, meaning they will be running with increased security and not be able to make critical changes to the system.

The  `useradd`command will create a user with the default permissions. Run the command in the terminal to add a new user called  `lowprivuser`. This user can be called anything.

`useradd -m -d /home/lowprivuser -p $(openssl passwd -1 password) lowprivuser`

Using`sudo su, it's possible to switch to running as this new, low privileged user.

`sudo su lowprivuser`

When running as this user, a couple of items change. For example, the user is not able to create or change files in certain locations such as the root directory,  `touch /root/blocked`.

The user is also not able to access Docker as previously this required them to have root permissions.

`docker ps`

In the next step, we'll deploy the new Rootless version and allow users launch their own containers.

#### Step 2 - Install Rootless Docker

Docker have made available a script which will deploy the required components for the new Rootless version.

Run the following command as  _lowprivuser_  to execute the script and install the components.

`curl -sSL https://get.docker.com/rootless | sh`

After this has finished, proceed to the next step to setup the user environment and start launching containers.

#### Step 3 - Access Docker

Rootless Docker has now been installed. The daemon can be started using the following script:

```
export XDG_RUNTIME_DIR=/tmp/docker-1001
export PATH=/home/lowprivuser/bin:$PATH
export DOCKER_HOST=unix:///tmp/docker-1001/docker.sock
mkdir -p $XDG_RUNTIME_DIR

/home/lowprivuser/bin/dockerd-rootless.sh --experimental --storage-driver vfs
```

This will run in the foreground and allow you to see the debug output from the Rootless Docker Daemon.

Click the following command to launch a second terminal window and change the context to run as roootlessuser.

```
sudo su lowprivuser
: "Second Terminal running as lowprivuser"
id
```

To access Docker, set the following environment variables. This specifies connecting to the Docker instance running for the user with id  _1001_, which should match the id of  _lowprivuser_.

```
export XDG_RUNTIME_DIR=/tmp/docker-1001
export PATH=/home/lowprivuser/bin:$PATH
export DOCKER_HOST=unix:///tmp/docker-1001/docker.sock
```

In the next step we can start launching containers.

#### Step 4 - Run Containers

It's now possible to access the Docker Daemon running for user 1001.

The standard Docker CLI commands work in the same way. The following command lists all the containers running for the user, currently it should return an empty list.

`docker ps`

It's possible to inspect details of the Daemon running:

`docker info`

Containers can be in the same way.

`docker run -it ubuntu bash`

Users within the container will still be reported as root. They will be able to install packages and modify parts of the system running inside of the Docker. However, if they managed to break out they wouldn't be able to interfer with the host.

`id`

In a separate terminal window, as root it's possible to explore which processes are running and which user started them. Using  _ps aux_  you can verify that our new container instance is managed and owned by our low privileged user.

`id; ps aux | grep lowprivuser`

The system is now running Docker Containers without requiring any additionals permissions, allowing our systems to operatoe with increased security.

You have successfully deployed the Rootless version of Docker and started containers without requiring Root access!

Run the script on your own Linux system to try it.

`curl -sSL https://get.docker.com/rootless | sh`

Continue learning more about Docker and containers at  [https://katacoda.com/courses/docker](https://katacoda.com/courses/docker)

#### Complete Command
```
$ useradd -m -d /home/lowprivuser -p $(openssl passwd -1 password) lowprivuser
$ sudo su lowprivuser
lowprivuser@host01:/root$ touch /root/blocked
touch: cannot touch '/root/blocked': Permission denied
lowprivuser@host01:/root$ docker ps
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.40/containers/json: dial unix /var/run/docker.sock: connect: permission denied
lowprivuser@host01:/root$ curl -sSL https://get.docker.com/rootless | sh
# Installing stable version 20.10.16
# Executing docker rootless install script, commit: ee3e0e0
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 61.9M  100 61.9M    0     0  92.8M      0 --:--:-- --:--:-- --:--:-- 92.7M
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 18.2M  100 18.2M    0     0  62.1M      0 --:--:-- --:--:-- --:--:-- 62.2M
+ PATH=/home/lowprivuser/bin:/root/go/bin:/usr/local/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games /home/lowprivuser/bin/dockerd-rootless-setuptool.sh install
[INFO] systemd not detected, dockerd-rootless.sh needs to be started manually:

PATH=/home/lowprivuser/bin:/sbin:/usr/sbin:$PATH dockerd-rootless.sh 

[INFO] Creating CLI context "rootless"
Successfully created context "rootless"

[INFO] Make sure the following environment variables are set (or add them to ~/.bashrc):

# WARNING: systemd not found. You have to remove XDG_RUNTIME_DIR manually on every logout.
export XDG_RUNTIME_DIR=/home/lowprivuser/.docker/run
export PATH=/home/lowprivuser/bin:$PATH
export DOCKER_HOST=unix:///home/lowprivuser/.docker/run/docker.sock

lowprivuser@host01:/root$ 
```