In the previous scenarios we've launched some containers, but like any process, containers can crash. This scenario will explore how you can keep containers live and automatically restart them if the crash unexpectedly.

#### Step 1 - Stop On Fail

Docker considers any containers to exit with a non-zero exit code to have crashed. By default a crashed container will remain stopped.

#### Example

We've created a special container which outputs a message and then exits with code 1 to simulate a crash.

You can launch an instance using  `docker run -d --name restart-default scrapbook/docker-restart-example`

If you list all the containers, including stopped, you will see the container has crashed  `docker ps -a`

While the logs will output our message, which in real-life would hopefully indicate information to help us diagnose the issue.

`docker logs restart-default`

#### Step 2 - Restart On Fail

Depending on your scenario, restarting a failed process might correct the problem. Docker can automatically retry to launch the Docker a specific number of times before it stops trying.

#### Example

The option  _--restart=on-failure:#_  allows you to say how many times Docker should try again. In the example below, Docker will restart the container three times before stopping.

`docker run -d --name restart-3 --restart=on-failure:3 scrapbook/docker-restart-example`

As we can see from the logs, it was launched on three occasions.

`docker logs restart-3`

#### Step 3 - Always Restart

Finally Docker can always restart a failed container, in this case, Docker will keep trying until the container it is explicitly told to stop.

#### Example

Use the  _always_  flag to automatically restart the container when is crashes for example  `docker run -d --name restart-always --restart=always scrapbook/docker-restart-example`

You can view the restart attempting via the log  `docker logs restart-always`

#### Complete Command
```
$ docker run -d --name restart-default scrapbook/docker-restart-example
c3b14c44e994afd9b0c468ce0c4d9e87e87c660b5b3c936ac61527f4b1ba5304
$ docker ps -a
CONTAINER ID        IMAGE                              COMMAND                  CREATED             STATUS                     PORTS               NAMES
c3b14c44e994        scrapbook/docker-restart-example   "/bin/sh -c ./launch…"   4 seconds ago       Exited (1) 2 seconds ago                       restart-default
$ docker ps -a
CONTAINER ID        IMAGE                              COMMAND                  CREATED             STATUS                     PORTS               NAMES
c3b14c44e994        scrapbook/docker-restart-example   "/bin/sh -c ./launch…"   8 seconds ago       Exited (1) 6 seconds ago                       restart-default
$ docker logs restart-default
Thu May 19 04:44:42 UTC 2022 Booting up...
$ docker run -d --name restart-3 --restart=on-failure:3 scrapbook/docker-restart-example
2822d4cd47286ae343a50e1e36d46e51adda0969467b81a8e10bf7c3788e76be
$ docker logs restart-3
Thu May 19 04:45:06 UTC 2022 Booting up...
Thu May 19 04:45:07 UTC 2022 Booting up...
$ docker run -d --name restart-always --restart=always scrapbook/docker-restart-example
c6d103dd99ca9a8339d1b3ab078c5fa9a9590bbde33f02921b7d6fd24f6567d1
$ docker logs restart-always
Thu May 19 04:45:20 UTC 2022 Booting up...
Thu May 19 04:45:21 UTC 2022 Booting up...
$ docker ps -a
CONTAINER ID        IMAGE                              COMMAND                  CREATED             STATUS                         PORTS               NAMES
c6d103dd99ca        scrapbook/docker-restart-example   "/bin/sh -c ./launch…"   15 seconds ago      Restarting (1) 2 seconds ago                       restart-always
2822d4cd4728        scrapbook/docker-restart-example   "/bin/sh -c ./launch…"   29 seconds ago      Exited (1) 21 seconds ago                          restart-3
c3b14c44e994        scrapbook/docker-restart-example   "/bin/sh -c ./launch…"   52 seconds ago      Exited (1) 49 seconds ago                          restart-default
$
```