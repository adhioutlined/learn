
This scenario will explore the different ways you can handle logging output from your application and services when running as containers.

#### Step 1 - Docker Logs

When you start a container, Docker will track the Standard Out and Standard Error outputs from the process and make them available via the client.

#### Example

In the background, there is an instance of Redis running with the name  _redis-server_. Using the Docker client, we can access the standard out and standard error outputs using  `docker logs redis-server`

#### Step 2 - SysLog

By default, the Docker logs are outputting using the  _json-file_  logger meaning the output stored in a JSON file on the host. This can result in large files filling the disk. As a result, you can change the log driver to move to a different destination.

#### Syslog

The Syslog log driver will write all the container logs to the central syslog on the host. "syslog is a widely used standard for message logging. It permits separation of the software that generates messages, the system that stores them, and the software that reports and analyses them."  [Wikipedia](https://en.wikipedia.org/wiki/Syslog)

This log-driver is designed to be used when syslog is being collected and aggregated by an external system.

#### Example

The command below will redirect the redis logs to syslog.

`docker run -d --name redis-syslog --log-driver=syslog redis`

#### Accessing Logs

If you attempted to view the logs using the client you'll recieve the error  _FATA[0000] "logs" command is supported only for "json-file" logging driver_

Instead, you need to access them via the syslog stream.

#### Step 3 - Disable Logging

The third option is to disable logging on the container. This is particularly useful for containers which are very verbose in their logging.

#### Example

When the container is launched simply set the log-driver to none. No output will be logged.

`docker run -d --name redis-none --log-driver=none redis`

#### Which Config?

The  _inspect_  command allows you to identify the logging configuration for a particular container. The command below will output the LogConfig section for each of the containers.

Server created in step 1

`docker inspect --format '{{ .HostConfig.LogConfig }}' redis-server`

Server created in step 2

`docker inspect --format '{{ .HostConfig.LogConfig }}' redis-syslog`

Server created in this step

`docker inspect --format '{{ .HostConfig.LogConfig }}' redis-none`

#### Complete Command
```
Your Interactive Bash Terminal. A safe place to learn and execute commands.

$ docker run -d --name redis-server redis
e8cfe2cf7933685ff4ec494d5c48283906b1303c095742e4ccbe07463b3ffc2e
$ 
$ 
$ ls
$ ls -alh
total 12K
drwxr-xr-x 2 scrapbook scrapbook 4.0K Apr 14  2019 .
drwxr-xr-x 1 scrapbook scrapbook 4.0K Apr 14  2019 ..
$ docker logs redis-server
1:C 19 May 04:40:38.005 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
1:C 19 May 04:40:38.005 # Redis version=4.0.8, bits=64, commit=00000000, modified=0, pid=1, just started
1:C 19 May 04:40:38.005 # Warning: no config file specified, using the default config. In order to specify a config file use redis-server /path/to/redis.conf
1:M 19 May 04:40:38.006 * Running mode=standalone, port=6379.
1:M 19 May 04:40:38.007 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
1:M 19 May 04:40:38.007 # Server initialized
1:M 19 May 04:40:38.007 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
1:M 19 May 04:40:38.007 # WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.
1:M 19 May 04:40:38.007 * Ready to accept connections
$ docker run -d --name redis-syslog --log-driver=syslog redis
6b6860a3e4951fe8bbe75751ebf59378e01a6bc851200eb0166ec1180efa21f2
$ docker run -d --name redis-none --log-driver=none redis
8b6f307fd60e52844e305c282662b485568f31774d31a5a6fa54cc0c040cf224
$ docker inspect --format '{{ .HostConfig.LogConfig }}' redis-server
{json-file map[]}
$ docker inspect --format '{{ .HostConfig.LogConfig }}' redis-syslog
{syslog map[]}
$ docker inspect --format '{{ .HostConfig.LogConfig }}' redis-none
{none map[]}
$
```