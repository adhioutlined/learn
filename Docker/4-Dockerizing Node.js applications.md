This scenario continues to explore how to build and deploy your applications as a Docker container. The previous scenario covered deploying a static HTML website. This scenario explores how to deploy a Node.js application within a container.

The environment is configured with access to a personal Docker instance, and the code for a default Expressjs application is in the working directory. To view the code use  `ls`  and  `cat <filename>`  or use the editor.

The machine name Docker is running on is called  _docker_. If you want to access any of the services then use  _docker_  instead of localhost or 0.0.0.0.
#### Step 1 - Base Image

As we described in the previous scenario, all images started with a base image, ideally as close to your desired configuration as possible. Node.js has pre-built images available with tags for each released version.

The image for Node 10.0 is node:10-alpine. This is an Alpine-based build which is smaller and more streamlined than the official image.

Alongside the base image, we also need to create the base directories of where the application runs from. Using the  _RUN <command>_  we can execute commands as if they're running from a command shell, by using  `mkdir`  we can create the directories where the application will execute from. In this case, an ideal directory would be  _/src/app_  as the environment user has read/write access to this directory.

We can define a working directory using  _WORKDIR <directory>_  to ensure that all future commands are executed from the directory relative to our application.

#### Task: Define Base Environment

Set the  _FROM <image>:<tag>_,  _RUN <command>_  and  _WORKDIR <directory>_  on separate lines to configure the base environment for deploying your application.

```
FROM node:10-alpine 
RUN mkdir -p /src/app 
WORKDIR /src/app
```
#### Step 2 - NPM Install

In the previous set, we configured the foundation of our configuration and how we want the application to be deployed. The next stage is to install the dependencies required to run the application. For Node.js this means running NPM install.

To keep build times to a minimum, Docker caches the results of executing a line in the Dockerfile for use in a future build. If something has changed, then Docker will invalidate the current and all following lines to ensure everything is up-to-date.

With NPM we only want to re-run  `npm install`  if something within our  _package.json_  file has changed. If nothing has changed then we can use the cache version to speed up deployment. By using  _COPY package.json <dest>_  we can cause the  _RUN npm install_  command to be invalidated if the  _package.json_  file has changed. If the file has not changed, then the cache will not be invalidated, and the cached results of the npm install command will be used.

#### Task: Add Dockerfile Lines

The following two lines are required in order Dockerfile to run npm install.
```
Copy to EditorCOPY package.json /src/app/package.json 
RUN npm install 
```

Copy the lines to the Dockerfile now so they can be used in the build later.

#### Protip

If you don't want to use the cache as part of the build then set the option  _--no-cache=true_  as part of the  `docker build`  command.

```
COPY package.json /src/app/package.json 
RUN npm install
```
#### Step 3 - Configuring Application

After we've installed our dependencies, we want to copy over the rest of our application's source code. Splitting the installation of the dependencies and copying out source code enables us to use the cache when required.

If we copied our code before running  _npm install_  then it would run every time as our code would have changed. By copying just package.json we can be sure that the cache is invalidated only when our package contents have changed.

#### Task: Deploy Application

Create the desired steps in the Dockerfile to finish the deployment of the application.

We can copy the entire directory where our Dockerfile is using  _COPY . <dest dir>_.

Once the source code has been copied, the ports the application requires to be accessed is defined using  _EXPOSE <port>_.

Finally, the application needs to be started. One neat trick when using Node.js is to use the  _npm start_  command. This looks in the  _package.json_  file to know how to launch the application saving duplication of commands.

In the next step, we'll build and launch the image.

```
COPY . /src/app 
EXPOSE 3000

CMD [ "npm", "start" ]
```
#### Step 4 - Building & Launching Container

To launch your application inside the container you first need to build an image.

#### Example: Build & Launch

The command to build the image is  `docker build -t my-nodejs-app .`

The command to launch the built image is  `docker run -d --name my-running-app -p 3000:3000 my-nodejs-app`

#### Testing Container

You can test the container is accessible using curl. If the application responds then you know that everything has correctly started.

`curl http://docker:3000`

#### Step 5 - Environment Variables

Docker images should be designed that they can be transferred from one environment to the other without making any changes or requiring to be rebuilt. By following this pattern you can be confident that if it works in one environment, such as staging, then it will work in another, such as production.

With Docker, environment variables can be defined when you launch the container. For example with Node.js applications, you should define an environment variable for  _NODE_ENV_  when running in production.

Using  _-e_  option, you can set the name and value as  _-e NODE_ENV=production_

#### Example

`docker run -d --name my-production-running-app -e NODE_ENV=production -p 3000:3000 my-nodejs-app`




#### Complete Command
```
Your Interactive Bash Terminal. A safe place to learn and execute commands.
$ 
$ docker build -t my-nodejs-app .
Sending build context to Docker daemon  17.92kB
Step 1/5 : FROM node:10-alpine
10-alpine: Pulling from library/node
ddad3d7c1e96: Pull complete 
de915e575d22: Pull complete 
7150aa69525b: Pull complete 
d7aa47be044e: Pull complete 
Digest: sha256:dc98dac24efd4254f75976c40bce46944697a110d06ce7fa47e7268470cf2e28
Status: Downloaded newer image for node:10-alpine
 ---> aa67ba258e18
Step 2/5 : RUN mkdir -p /src/app
 ---> Running in c2115b66910a
Removing intermediate container c2115b66910a
 ---> d8392cab4ca6
Step 3/5 : WORKDIR /src/app
Removing intermediate container 51f34e6dbfba
 ---> c85949f7696f
Step 4/5 : COPY package.json /src/app/package.json
 ---> bd800e198f71
Step 5/5 : RUN npm install
 ---> Running in 96429af61f4a
npm WARN deprecated jade@1.6.0: Jade has been renamed to pug, please install the latest version of pug instead of jade
npm WARN deprecated transformers@2.1.0: Deprecated, use jstransformer
npm WARN deprecated constantinople@2.0.1: Please update to at least constantinople 3.1.1
npm notice created a lockfile as package-lock.json. You should commit this file.
added 77 packages from 74 contributors and audited 78 packages in 3.791s
found 23 vulnerabilities (4 low, 5 moderate, 12 high, 2 critical)
  run `npm audit fix` to fix them, or `npm audit` for details
Removing intermediate container 96429af61f4a
 ---> 0fe68ce2fd75
Successfully built 0fe68ce2fd75
Successfully tagged my-nodejs-app:latest
$ 
Your Interactive Bash Terminal. A safe place to learn and execute commands.
$ 
$ docker build -t my-nodejs-app .
Sending build context to Docker daemon  17.92kB
Step 1/5 : FROM node:10-alpine
10-alpine: Pulling from library/node
ddad3d7c1e96: Pull complete 
de915e575d22: Pull complete 
7150aa69525b: Pull complete 
d7aa47be044e: Pull complete 
Digest: sha256:dc98dac24efd4254f75976c40bce46944697a110d06ce7fa47e7268470cf2e28
Status: Downloaded newer image for node:10-alpine
 ---> aa67ba258e18
Step 2/5 : RUN mkdir -p /src/app
 ---> Running in c2115b66910a
Removing intermediate container c2115b66910a
 ---> d8392cab4ca6
Step 3/5 : WORKDIR /src/app
Removing intermediate container 51f34e6dbfba
 ---> c85949f7696f
Step 4/5 : COPY package.json /src/app/package.json
 ---> bd800e198f71
Step 5/5 : RUN npm install
 ---> Running in 96429af61f4a
npm WARN deprecated jade@1.6.0: Jade has been renamed to pug, please install the latest version of pug instead of jade
npm WARN deprecated transformers@2.1.0: Deprecated, use jstransformer
npm WARN deprecated constantinople@2.0.1: Please update to at least constantinople 3.1.1
npm notice created a lockfile as package-lock.json. You should commit this file.
added 77 packages from 74 contributors and audited 78 packages in 3.791s
found 23 vulnerabilities (4 low, 5 moderate, 12 high, 2 critical)
  run `npm audit fix` to fix them, or `npm audit` for details
Removing intermediate container 96429af61f4a
 ---> 0fe68ce2fd75
Successfully built 0fe68ce2fd75
Successfully tagged my-nodejs-app:latest
$ docker run -d --name my-running-app -p 3000:3000 my-nodejs-app
76c97a899147153c5ee0afe95156ab22fba0cffd295701381b06488e8bc8a6e6
$ curl http://docker:3000
curl: (7) Failed to connect to docker port 3000: Connection refused
$ curl http://docker:3000
curl: (7) Failed to connect to docker port 3000: Connection refused
$

$ docker run -d --name my-production-running-app -e NODE_ENV=production -p 3000:3000 my-nodejs-app
6662c3ca69c1c118551778cbbcf7dc03b1ef5fe377daf648cbf78e3d7722d3ff

```