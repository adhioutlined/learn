Docker is an open source project that automates the deployment of applications using containers, providing an additional layer of abstraction and automation on top of Linux.

This playground provides a personalised sandboxed environment for you to learn and explore Docker.

#### What are playgrounds?

Playgrounds give you a configured environment to start playing and exploring using an unstructured learning approach.

Playgrounds are great for experimenting and trying samples. To learn more about the technology then start with one of our  [labs](https://www.katacoda.com/learn)


#### Create Swarm Cluster

## Initialize

`docker swarm init`

## Join

`token=$(ssh -o StrictHostKeyChecking=no 10.0.0.39 "docker swarm join-token -q worker") && echo $token`

`docker swarm join 10.0.0.39:2377 --token $token`

## Create Overlay Network

`docker network create -d overlay skynet`


#### Complete Command Host-1
```
$ docker swarm init
Swarm initialized: current node (j7uen4w9yw6qynfum6j7j7o6c) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-0xnctxkfmjxf2fyp3az0y4djnf02sif7bx21evqgipaub0t93k-2ijcukm3gogb8xqsjc9uzhrh8 10.0.0.39:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

$ 
$ docker network create -d overlay skynet
q9dmvob34mgu7j6i4j9u22l5u
$ 
```
#### Complete Command Host-2
```
$ token=$(ssh -o StrictHostKeyChecking=no 10.0.0.39 "docker swarm join-token -q worker") && echo $token
Warning: Permanently added '10.0.0.39' (ECDSA) to the list of known hosts.
SWMTKN-1-0xnctxkfmjxf2fyp3az0y4djnf02sif7bx21evqgipaub0t93k-2ijcukm3gogb8xqsjc9uzhrh8
$ docker swarm join 10.0.0.39:2377 --token $token
This node joined a swarm as a worker.
$ 
```