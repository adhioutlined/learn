In this scenario, we'll explore how you can ignore certain files from ending up inside a Docker image that can introduce security risks. This scenario also investigates how you can reduce the build time by ignoring which files are sent to the Docker Build Context.

#### Step 1 - Docker Ignore

To prevent sensitive files or directories from being included by mistake in images, you can add a file named  _.dockerignore_.

#### Example

A Dockerfile copies the working directory into the Docker Image. As a result, this would include potentially sensitive information such as a passwords file which we'd want to manage outside the image. View the Dockerfile with  `cat Dockerfile`

Build the image with  `docker build -t password .`

Look at the output using  `docker run password ls /app`

This will include the passwords file.

## Ignore File

The following command would include passwords.txt in our  _.dockerignore_  file and ensure that it didn't accidentally end up in a container. The  _.dockerignore_  file would be stored in source control and share with the team to ensure that everyone is consistent.

`echo passwords.txt >> .dockerignore`

The ignore file supports directories and Regular expressions to define the restrictions, very similar to  _.gitignore_. This file can also be used to improve build times which we'll investigate in the next step.

Build the image, because of the Docker Ignore file it shouldn't include the passwords file.

`docker build -t nopassword .`

Look at the output using  `docker run nopassword ls /app`

#### Protip

If you need to use the passwords as part of a  _RUN_  command then you need to copy, execute and delete the files as part of a single RUN command. Only the final state of the Docker container is persisted inside the image.

#### Step 2 - Docker Build Context

The  _.dockerignore_  file can ensure that sensitive details are not included in a Docker Image. However they can also be used to improve the build time of images.

In the environment, a 100M temporary file has been created. This file is never used by the  _Dockerfile_. When you execute a build command, Docker sends the entire path contents to the Engine for it to calculate which files to include. As a result sending the 100M file is unrequired and creates a slower build.

You can see the 100M impact by executing following the command.

`docker build -t large-file-context .`

In the next step, we'll demonstrate how to improve the performance of the build.

#### Protip

It's wise to ignore  _.git_  directories along with dependencies that are downloaded/built within the image such as  _node_modules_. These are never used by the application running within the Docker Container and just add overhead to the build process.

#### Step 3 - Optimised Build

In the same way, we used the  _.dockerignore_  file to exclude sensitive files, we can use it to exclude files which we don't want to be sent to the Docker Build Context during the build.

#### Optimizing

To speed up our build, simply include the filename of the large file in the ignore file.

`echo big-temp-file.img >> .dockerignore`

When we rebuild the image, it will be much faster as it doesn't have to copy the 100M file.

`docker build -t no-large-file-context .`

This optimisation has a greater impact when ignoring large directories such as  _.git_.


#### Complete Command
```
$ cat Dockerfile
FROM alpine
ADD . /app
COPY cmd.sh /cmd.sh
CMD ["sh", "-c", "/cmd.sh"]
$ docker build -t password .
Sending build context to Docker daemon  4.096kB
Step 1/4 : FROM alpine
 ---> 3fd9065eaf02
Step 2/4 : ADD . /app
 ---> 84e3cb7b0174
Step 3/4 : COPY cmd.sh /cmd.sh
 ---> e1e4897bd1af
Step 4/4 : CMD ["sh", "-c", "/cmd.sh"]
 ---> Running in 7c53892b5712
Removing intermediate container 7c53892b5712
 ---> 075892c147ec
Successfully built 075892c147ec
Successfully tagged password:latest
$ docker run password ls /app
Dockerfile
cmd.sh
passwords.txt
$ echo passwords.txt >> .dockerignore
$ docker build -t nopassword .
Sending build context to Docker daemon  4.096kB
Step 1/4 : FROM alpine
 ---> 3fd9065eaf02
Step 2/4 : ADD . /app
 ---> a7da17a7e986
Step 3/4 : COPY cmd.sh /cmd.sh
 ---> ed64ae3707c2
Step 4/4 : CMD ["sh", "-c", "/cmd.sh"]
 ---> Running in 4a593e767a6d
Removing intermediate container 4a593e767a6d
 ---> 264223e06542
Successfully built 264223e06542
Successfully tagged nopassword:latest
$ docker run nopassword ls /app
Dockerfile
cmd.sh
$ 
$ docker build -t large-file-context .
Sending build context to Docker daemon  104.9MB
Step 1/4 : FROM alpine
 ---> 3fd9065eaf02
Step 2/4 : ADD . /app
 ---> 54eb4a454dad
Step 3/4 : COPY cmd.sh /cmd.sh
 ---> 43bee800dc46
Step 4/4 : CMD ["sh", "-c", "/cmd.sh"]
 ---> Running in 6cc4577096f2
Removing intermediate container 6cc4577096f2
 ---> 872cf31f772d
Successfully built 872cf31f772d
Successfully tagged large-file-context:latest
$ 

```