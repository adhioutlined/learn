
In this scenario you will learn how to use the multi-stage build functionality to make smaller, more optimised images.

The feature is ideal for deploying languages such as Golang as containers. By having multi-stage builds, the first stage can build the Golang binary using a larger Docker image as the base. In the second stage, the newly built binary can be deployed using a much smaller base image. The end result is an optimised Docker Image.

#### Create Dockerfile

The Multi-Stage feature allows a single Dockerfile to contain multiple stages in order to produce the desired, optimised, Docker Image.

Previously, the problem would have been solved with two Dockerfiles. One file would have the steps to build the binary and artifacts using a development container, the second would be optimised for production and not include the development tools.

By removing development tooling in the production image, you reproduce the attack surface and improve the deployment time.

## Sample Code

Start by deploying a sample  [Golang HTTP Server](https://github.com/katacoda/golang-http-server). This currently using a two staged Docker Build approach. This scenario will create a new Dockerfile that allows the image to be built using a single command.

`git clone https://github.com/katacoda/golang-http-server.git`

## Multi-Stage Dockerfile

Using the editor, create a Multi-Stage Dockerfile. The first stage using the Golang SDK to build a binary. The second stage copies the resulting binary into a optimised Docker Image.

Copy to Editor
```
# First Stage FROM golang:1.6-alpine

RUN mkdir /app ADD . /app/ WORKDIR /app RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main . 
# Second Stage

FROM alpine
EXPOSE 80
CMD ["/app"] 

# Copy from first stage

COPY --from=0 /app/main /app 
```
There are currently discussions about improving the syntax that you can follow at  [https://github.com/docker/docker/pull/31257](https://github.com/docker/docker/pull/31257)

#### Build Multi-Stage Docker Image

With the new syntax for the Dockerfile in place, the build process is identical to previously.

## Task

Create the desired Docker Image using the build command below.

`docker build -f Dockerfile.multi -t golang-app .`

The result will be two images. One untagged that was used for the first stage and the second, smaller image, our target image.

`docker images`

If you receive the error,  _COPY --from=0 /build/out /app/ Unknown flag: from_, it means you're running an older version of Docker without the multi-stage support. Step 1 of this scenario upgrades the current Docker version.

#### Test Image

The image can be launched and deployed without any changes required.

`docker run -d -p 80:80 golang-app`

`curl localhost`



```
[root@host01 ~]# git clone https://github.com/katacoda/golang-http-server.git
Cloning into 'golang-http-server'...
remote: Enumerating objects: 82, done.
remote: Counting objects: 100% (19/19), done.
remote: Compressing objects: 100% (8/8), done.
remote: Total 82 (delta 11), reused 11 (delta 11), pack-reused 63
Unpacking objects: 100% (82/82), done.
[root@host01 ~]# cd golang-http-server/
[root@host01 golang-http-server]# 
[root@host01 golang-http-server]# 
[root@host01 golang-http-server]# docker build -f Dockerfile.multi -t golang-app .
Sending build context to Docker daemon  2.101MB
Step 1/9 : FROM golang:1.6-alpine
1.6-alpine: Pulling from library/golang
b7f33cc0b48e: Pull complete 
91365fe6b6b6: Pull complete 
a7f35c05c6f8: Pull complete 
f92b4d3b8ab3: Pull complete 
6973cd4e099e: Pull complete 
6930f3feba46: Pull complete 
60124a1a7c2c: Pull complete 
Digest: sha256:269d188232cd9a6194f71650780cb2e903a76958182def1008cc7255f6f457d6
Status: Downloaded newer image for golang:1.6-alpine
 ---> 1ea38172de32
Step 2/9 : RUN mkdir /app
 ---> Running in 029c561ce4dd
Removing intermediate container 029c561ce4dd
 ---> 2080802644d5
Step 3/9 : ADD . /app/
 ---> 7613229fc498
Step 4/9 : WORKDIR /app
Removing intermediate container 22d227787c61
 ---> 38000b042401
Step 5/9 : RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
 ---> Running in 99f3a7acb7a9
Removing intermediate container 99f3a7acb7a9
 ---> 07b56aeb7dbb
Step 6/9 : FROM alpine
 ---> 3fd9065eaf02
Step 7/9 : EXPOSE 80
 ---> Running in ed9ba601728a
Removing intermediate container ed9ba601728a
 ---> 7f6302bb17db
Step 8/9 : CMD ["/app"]
 ---> Running in 7fb66775feae
Removing intermediate container 7fb66775feae
 ---> c23451a7f47c
Step 9/9 : COPY --from=0 /app/main /app
 ---> f72a0e5fd20e
Successfully built f72a0e5fd20e
Successfully tagged golang-app:latest
[root@host01 golang-http-server]# 
[root@host01 golang-http-server]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
golang-app          latest              f72a0e5fd20e        22 seconds ago      11.7MB
<none>              <none>              07b56aeb7dbb        25 seconds ago      293MB
redis               latest              4760dc956b2d        4 years ago         107MB
ubuntu              latest              f975c5035748        4 years ago         112MB
alpine              latest              3fd9065eaf02        4 years ago         4.14MB
golang              1.6-alpine          1ea38172de32        5 years ago         283MB
[root@host01 golang-http-server]# 
[root@host01 golang-http-server]# docker run -d -p 80:80 golang-app
3c804e8aaca39a30b1c5ebd51aeba1c3cbf515025adc0bd1e68ae9ca284670be
[root@host01 golang-http-server]# curl localhost
<h1>This request was processed by host: 3c804e8aaca3</h1>
[root@host01 golang-http-server]# 
```