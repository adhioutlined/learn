This scenario explores how to create a  _docker network_  allowing containers to communicate. We'll also explore the Embedded DNS Server added in Docker 1.10.

Docker has two approaches to networking. The first defines a link between two containers. This link updates  _/etc/hosts_  and environment variables to allow containers to discover and communicate.

The alternate approach is to create a  _docker network_  that containers are connected to. The network has similar attributes to a physical network, allowing containers to come and go more freely than when using links.

#### Step 1 - Create Network

The first step is to create a network using the CLI. This network will allow us to attach multiple containers which will be able to discover each other.

In this example, we're going to start by creating a  _backend-network_. All containers attached to our backend will be on this network.

## Task: Create Network

To start with we create the network with our predefined name.  `docker network create backend-network`

## Task: Connect To Network

When we launch new containers, we can use the  _--net_  attribute to assign which network they should be connected to.  `docker run -d --name=redis --net=backend-network redis`

In the next step we'll explore the state of the network.

#### Step 2 - Network Communication

Unlike using links,  _docker network_  behave like traditional networks where nodes can be attached/detached.

## Task: Explore

The first thing you'll notice is that Docker no longer assigns environment variables or updates the hosts file of containers. Explore using the following two commands and you'll notice it no longer mentions other containers.

`docker run --net=backend-network alpine env`

`docker run --net=backend-network alpine cat /etc/hosts`

Instead, the way containers can communicate via an Embedded DNS Server in Docker. This DNS server is assigned to all containers via the IP 127.0.0.11 and set in the  _resolv.conf_  file.

`docker run --net=backend-network alpine cat /etc/resolv.conf`

When containers attempt to access other containers via a well-known name, such as Redis, the DNS server will return the IP address of the correct Container. In this case, the fully qualified name of Redis will be  _redis.backend-network_.

`docker run --net=backend-network alpine ping -c1 redis`

#### Step 3 - Connect Two Containers

Docker supports multiple networks and containers being attached to more than one network at a time.

For example, let's create a separate network with a Node.js application that communicates with our existing Redis instance.

# Task

The first task is to create a new network in the same way.

`docker network create frontend-network`

When using the  _connect_  command it is possible to attach existing containers to the network.

`docker network connect frontend-network redis`

When we launch the web server, given it's attached to the same network it will be able to communicate with our Redis instance.

`docker run -d -p 3000:3000 --net=frontend-network katacoda/redis-node-docker-example`

You can test it using  `curl docker:3000`

#### Step 4 - Create Aliases

Links are still supported when using  _docker network_  and provide a way to define an Alias to the container name. This will give the container an extra DNS entry name and way to be discovered. When using --link the embedded DNS will guarantee that localised lookup result only on that container where the --link is used.

The other approach is to provide an alias when connecting a container to a network.

## Connect Container with Alias

The following command will connect our Redis instance to the frontend-network with the alias of  _db_.

`docker network create frontend-network2 docker network connect --alias db frontend-network2 redis`

When containers attempt to access a service via the name db, they will be given the IP address of our Redis container.

`docker run --net=frontend-network2 alpine ping -c1 db`

#### Step 5 - Disconnect Containers

With our networks created, we can use the CLI to explore the details.

The following command will list all the networks on our host.

`docker network ls`

We can then explore the network to see which containers are attached and their IP addresses.

`docker network inspect frontend-network`

The following command disconnects the redis container from the  _frontend-network_.

`docker network disconnect frontend-network redis`

#### Complete Command
```
$ docker network create backend-network
6f2dc43967690287d91aa6ce131d415395a6d1dca8a504beaeb93ec05f740f1c
$ docker run -d --name=redis --net=backend-network redis
7d002293836188a97cf37b3d182f97b978c6e9bd5a1fc9fb9b8a2ec012bf046b
$ docker run --net=backend-network alpine env
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=3076bd1d007b
HOME=/root
$ docker run --net=backend-network alpine cat /etc/hosts
127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.17.0.3      eefdd4764d2e
$ docker run --net=backend-network alpine cat /etc/resolv.conf
nameserver 127.0.0.11
options ndots:0
$ docker run --net=backend-network alpine ping -c1 redis
PING redis (172.17.0.2): 56 data bytes
64 bytes from 172.17.0.2: seq=0 ttl=64 time=0.102 ms

--- redis ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 0.102/0.102/0.102 ms
$ docker network create frontend-network
9d56de3c0aa59c5f75ccd97545ff39d5fad6377685a1f6790416d2806bf3232b
$ docker network connect frontend-network redis
$ docker run -d -p 3000:3000 --net=frontend-network katacoda/redis-node-docker-example
Unable to find image 'katacoda/redis-node-docker-example:latest' locally
latest: Pulling from katacoda/redis-node-docker-example
12b41071e6ce: Pull complete 
a3ed95caeb02: Pull complete 
49a025abf7e3: Pull complete 
1fb1c0be01ab: Pull complete 
ae8c1f781cde: Pull complete 
db73207ad2ae: Pull complete 
446b13034c13: Pull complete 
Digest: sha256:1aae9759464f00953c8e078a0e0d0649622fef9dd5655b1491f9ee589ae904b4
Status: Downloaded newer image for katacoda/redis-node-docker-example:latest
c457a12b491b12a33f45d4ee8d356d3a7b4892f49aa71d650d95f53309a2d493
$ curl docker:3000
This page was generated after talking to redis.

Application Build: 1

Total requests: 1

IP count: 
    ::ffff:10.0.0.19: 1
$ docker network create frontend-network2
7ba6bacde467cc7aed3ff33f78e84fafa4567b9f59f34a739efba9e713079f07
$ docker network connect --alias db frontend-network2 redis
$ docker run --net=frontend-network2 alpine ping -c1 db
PING db (172.20.0.2): 56 data bytes
64 bytes from 172.20.0.2: seq=0 ttl=64 time=0.105 ms

--- db ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 0.105/0.105/0.105 ms
$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
6f2dc4396769        backend-network     bridge              local
888d67b057c1        bridge              bridge              local
9d56de3c0aa5        frontend-network    bridge              local
7ba6bacde467        frontend-network2   bridge              local
fa054a9af353        host                host                local
f50397115ef2        none                null                local
$ docker network inspect frontend-network
[
    {
        "Name": "frontend-network",
        "Id": "9d56de3c0aa59c5f75ccd97545ff39d5fad6377685a1f6790416d2806bf3232b",
        "Created": "2022-05-19T04:27:15.080245381Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.19.0.0/16",
                    "Gateway": "172.19.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "7d002293836188a97cf37b3d182f97b978c6e9bd5a1fc9fb9b8a2ec012bf046b": {
                "Name": "redis",
                "EndpointID": "798523bd80911299e1d5dccb3a523f190764a6f8312f21c1cefb92098750346f",
                "MacAddress": "02:42:ac:13:00:02",
                "IPv4Address": "172.19.0.2/16",
                "IPv6Address": ""
            },
            "c457a12b491b12a33f45d4ee8d356d3a7b4892f49aa71d650d95f53309a2d493": {
                "Name": "sad_elion",
                "EndpointID": "20c8bd7f424d910261d6ef3f02f53b5721aa685f1bdf76306cfc19832321adb3",
                "MacAddress": "02:42:ac:13:00:03",
                "IPv4Address": "172.19.0.3/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
$ docker network disconnect frontend-network redis
$ docker network disconnect frontend-network redis
Error response from daemon: container 7d002293836188a97cf37b3d182f97b978c6e9bd5a1fc9fb9b8a2ec012bf046b is not connected to network frontend-network
$ docker network inspect frontend-network
[
    {
        "Name": "frontend-network",
        "Id": "9d56de3c0aa59c5f75ccd97545ff39d5fad6377685a1f6790416d2806bf3232b",
        "Created": "2022-05-19T04:27:15.080245381Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.19.0.0/16",
                    "Gateway": "172.19.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "c457a12b491b12a33f45d4ee8d356d3a7b4892f49aa71d650d95f53309a2d493": {
                "Name": "sad_elion",
                "EndpointID": "20c8bd7f424d910261d6ef3f02f53b5721aa685f1bdf76306cfc19832321adb3",
                "MacAddress": "02:42:ac:13:00:03",
                "IPv4Address": "172.19.0.3/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
$ 
```