In the first scenario, we discussed how you could start containers based on pre-existing images from the Docker Registry. This scenario explains how to build an image based on your requirements.

For this scenario, the container will be running a static HTML application using Nginx, a high-performance web server. In the future, we'll explain how to deploy other stacks such as Node.js or ASP.NET.

The machine name Docker is running on is called  _docker_. If you want to access any of the services, then use  _docker_  instead of localhost or 0.0.0.0.

#### Docker Images

Docker images are built based on a Dockerfile. A Dockerfile defines all the steps required to create a Docker image with your application configured and ready to be run as a container. The image itself contains everything, from operating system to dependencies and configuration required to run your application.

Having everything within the image allows you to migrate images between different environments and be confident that if it works in one environment, then it will work in another.

The Dockerfile allows for images to be composable, enabling users to extend existing images instead of building from scratch. By building on an existing image, you only need to define the steps to setup your application. The base images can be basic operating system installations or configured systems which simply need some additional customisations.

To help you complete the steps, an environment has been created with Docker configured. The editor allows you to write a Dockerfile which defines how to build the Docker image.

#### Step 1 - Base Images

All Docker images start from a base image. A base image is the same images from the Docker Registry which are used to start containers. Along with the image name, we can also include the image tag to indicate which particular version we want, by default, this is latest.

These base images are used as the foundation for your additional changes to run your application. For example, in this scenario, we require NGINX to be configured and running on the system before we can deploy our static HTML files. As such we want to use NGINX as our base image.

Dockerfile's are simple text files with a command on each line. To define a base image we use the instruction  _FROM <image-name>:<tag>_

#### Task: Creating a Dockerfile

The first line of the Dockerfile should be  _FROM nginx:1.11-alpine_

Make the change in the Dockerfile editor. Within the environment, a new Dockerfile will be created with the contents of the editor.
```
FROM nginx:1.11-alpine
```

#### Caution

It's tempting to use the tag  _:latest_  however this can result in you building your image against a version which you were not expecting. We recommend that you always use a particular version number as your tag and manage the updating yourself.

#### Step 2 - Running Commands

With the base image defined, we need to run various commands to configure our image. There are many commands to help with this, the main commands two are  _COPY_  and  _RUN_.

_RUN <command>_  allows you to execute any command as you would at a command prompt, for example installing different application packages or running a build command. The results of the RUN are persisted to the image so it's important not to leave any unnecessary or temporary files on the disk as these will be included in the image.

_COPY <src> <dest>_  allows you to copy files from the directory containing the Dockerfile to the container's image. This is extremely useful for source code and assets that you want to be deployed inside your container.

#### Task

A new  _index.html_  file has been created for you which we want to serve from our container. On the next line after the FROM command, use the COPY command to copy index.html into a directory called /usr/share/nginx/html

#### Protip

If you're copying a file into a directory then you need to specify the filename as part of the destination.

```
COPY index.html /usr/share/nginx/html/index.html
```

#### Step 3 - Exposing Ports

With our files copied into our image and any dependencies downloaded, you need to define which port application needs to be accessible on.

Using the  _EXPOSE <port>_  command you tell Docker which ports should be open and can be bound to. You can define multiple ports on the single command, for example,  _EXPOSE 80 433_  or  _EXPOSE 7000-8000_

#### Task

We want our web server to be accessible via port 80, add the relevant  _EXPOSE_  line to the Dockerfile.

```
EXPOSE 80
```
#### Step 4 - Default Commands

With the Docker image configured and having defined which ports we want accessible, we now need to define the command that launches the application.

The  _CMD_  line in a Dockerfile defines the default command to run when a container is launched. If the command requires arguments then it's recommended to use an array, for example ["cmd", "-a", "arga value", "-b", "argb-value"], which will be combined together and the command  `cmd -a "arga value" -b argb-value`  would be run.

#### Task

The command to run NGINX is  `nginx -g daemon off;`. Set this as the default command in the Dockerfile.

#### Protip

An alternative approach to  _CMD_  is  _ENTRYPOINT_. While a CMD can be overridden when the container starts, a  _ENTRYPOINT_  defines a command which can have arguments passed to it when the container launches.

In this example, NGINX would be the entrypoint with -g daemon off; the default command.
```
CMD ["nginx", "-g", "daemon off;"]
```
#### Step 5 - Building Containers

After writing your Dockerfile you need to use  `docker build`  to turn it into an image. The  _build_  command takes in a directory containing the Dockerfile, executes the steps and stores the image in your local Docker Engine. If one fails because of an error then the build stops.

#### Task

Using the  `docker build`  command to build the image. You can give the image a friendly name by using the  _-t <name>_  option.

#### Protip

You can use  `docker images`  to see a list of the images on your local machine.

`docker build -t my-nginx-image:latest .`

#### Step 6 - Launching New Image

With the image successfully created, you can now launch the container in the same way we described in the first scenario.

#### Task

Launch an instance of your newly built image using either the ID result from the build command or the friendly name you assigned it.

NGINX is designed to run as a background service so you should include the option  _-d_. To make the web server accessible, bind it to port 80 using  _p 80:80_

For example:

`docker run -d -p 80:80 <image-id|friendly-tag-name>`

You can access the launched web server via the hostname  _docker_. After launching the container, the command  `curl -i http://docker`  will return our index file via NGINX and the image we built.

#### Protip

You can check the container is running using  `docker ps`
`docker run -d -p 80:80 my-nginx-image:latest`

#### Complete DockerFile
```
# This is your Editor pane. Write the Dockerfile here and 
# use the command line to execute commands

FROM nginx:1.11-alpine
COPY index.html /usr/share/nginx/html/index.html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```
#### Complete Command
```
Your Interactive Bash Terminal. A safe place to learn and execute commands.

$ docker build -t my-nginx-image:latest .
Sending build context to Docker daemon  3.072kB
Step 1/4 : FROM nginx:1.11-alpine
 ---> bedece1f06cc
Step 2/4 : COPY index.html /usr/share/nginx/html/index.html
 ---> d4a658cccdec
Step 3/4 : EXPOSE 80
 ---> Running in f54a7afa59fc
Removing intermediate container f54a7afa59fc
 ---> 903f42f1fa86
Step 4/4 : CMD ["nginx", "-g", "daemon off;"]
 ---> Running in 26a172688df1
Removing intermediate container 26a172688df1
 ---> b0668d2b32d7
Successfully built b0668d2b32d7
Successfully tagged my-nginx-image:latest
$ docker run -d -p 80:80 my-nginx-image:latest
77fd838acfecf9041355170f70e53106ac4edd6184d5110390070cabab46c013
$
```