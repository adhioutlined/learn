In this session, we'll explore how you can use the NGINX web server to load balance requests between two containers running on the host.

With Docker, there are two main ways for containers to communicate with each other. The first is via links which configure the container with environment variables and host entry allowing them to communicate. The second is using the Service Discovery pattern where uses information provided by third parties, in this scenario, it will be Docker's API.

The Service Discovery pattern is where the application uses a third party system to identify the location of the target service. For example, if our application wanted to talk to a database, it would first ask an API what the IP address of the database is. This pattern allows you to quickly reconfigure and scale your architectures with improved fault tolerance than fixed locations.

The machine name Docker is running on is called  _docker_. If you want to access any of the services, then use  _docker_  instead of localhost or 0.0.0.0.

#### Step 1 - NGINX Proxy

In this scenario, we want to have a NGINX service running which can dynamically discovery and update its load balance configuration when new containers are loaded. Thankfully this has already been created and is called  [nginx-proxy](https://github.com/jwilder/nginx-proxy).

Nginx-proxy accepts HTTP requests and proxies the request to the appropriate container based on the request Hostname. This is transparent to the user with happens without any additional performance overhead.

#### Properties

There are three keys properties required to be configured when launching the proxy container.

The first is binding the container to port 80 on the host using  _-p 80:80_. This ensures all HTTP requests are handled by the proxy.

The second is to mount the  _docker.sock_  file. This is a connection to the Docker daemon running on the host and allows containers to access its metadata via the API. Nginx-proxy uses this to listen for events and then updates the NGINX configuration based on the container IP address. Mounting file works in the same way as directories using  _-v /var/run/docker.sock:/tmp/docker.sock:ro_. We specify  _:ro_  to restrict access to read-only.

Finally, we can set an optional _-e DEFAULT_HOST=<domain>_. If a request comes in and doesn't make any specified hosts, then this is the container where the request will be handled. This enables you to run multiple websites with different domains on a single machine with a fall-back to a known website.

#### Task

Use the command below to launch nginx-proxy.

`docker run -d -p 80:80 -e DEFAULT_HOST=proxy.example -v /var/run/docker.sock:/tmp/docker.sock:ro --name nginx jwilder/nginx-proxy`

Because we're using a DEFAULT_HOST, any requests which come in will be directed to the container that has been assigned the HOST proxy.example.

#### Request

You can make a request to the web server using  `curl http://docker`. As we have no containers, it will return a 503 error.

#### Step 2 - Single Host

Nginx-proxy is now listening to events which Docker raises on start / stop. A sample website called  _katacoda/docker-http-server_  has been created which returns the machine name its running on. This allows us to test that our proxy is working as expected. Internally its a PHP and Apache2 application listening on port 80.

#### Starting Container

For Nginx-proxy to start sending requests to a container you need to specify the  _VIRTUAL_HOST_  environment variable. This variable defines the domain where requests will come from and should be handled by the container.

In this scenario we'll set our HOST to match our DEFAULT_HOST so it will accept all requests.

`docker run -d -p 80 -e VIRTUAL_HOST=proxy.example katacoda/docker-http-server`

#### Testing

Sometimes it takes a few seconds for NGINX to reload but if we execute a request to our proxy using  `curl http://docker`  then the request will be handled by our container.

#### Step 3 - Cluster

We now have successfully created a container to handle our HTTP requests. If we launch a second container with the same VIRTUAL_HOST then nginx-proxy will configure the system in a round-robin load balanced scenario. This means that the first request will go to one container, the second request to a second container and then repeat in a circle. There is no limit to the number of nodes you can have running.

#### Task

Launch a second container using the same command as we did before.

`docker run -d -p 80 -e VIRTUAL_HOST=proxy.example katacoda/docker-http-server`

#### Testing

If we execute a request to our proxy using  `curl http://docker`  then the request will be handled by our first container. A second HTTP request will return a different machine name meaning it was dealt with by our second container.

#### Step 4 - Generated NGINX Configuration

While  _nginx-proxy_  automatically creates and configures NGINX for us, if you're interested in what the final configuration looks like then you can output the complete config file with  _docker exec_  as shown below.

`docker exec nginx cat /etc/nginx/conf.d/default.conf`

Additional information about when it reloads configuration can be found in the logs using  `docker logs nginx`


#### Complete Command
```
$ docker run -d -p 80:80 -e DEFAULT_HOST=proxy.example -v /var/run/docker.sock:/tmp/docker.sock:ro --name nginx jwilder/nginx-proxy
dbcafd14b7411c313009c04dd2d504101df90a9f1976782bf62f8518202c946f
$ curl http://docker
<html>
<head><title>503 Service Temporarily Unavailable</title></head>
<body>
<center><h1>503 Service Temporarily Unavailable</h1></center>
<hr><center>nginx</center>
</body>
</html>
$ docker run -d -p 80 -e VIRTUAL_HOST=proxy.example katacoda/docker-http-server
4635af625132c84788e1fefc5e02369f236df8ac5bdd61352e3ed7bc14334cb5
$ curl http://docker
<h1>This request was processed by host: 4635af625132</h1>
$ docker run -d -p 80 -e VIRTUAL_HOST=proxy.example katacoda/docker-http-server
aa5e40dae6925991fa6cb78231e293c0cb7d95d38c0a1f8a96e99222e0d0e397
$ curl http://docker
<h1>This request was processed by host: aa5e40dae692</h1>
$ docker exec nginx cat /etc/nginx/conf.d/default.conf
# nginx-proxy version : 1.0.1-6-gc4ad18f
# If we receive X-Forwarded-Proto, pass it through; otherwise, pass along the
# scheme used to connect to this server
map $http_x_forwarded_proto $proxy_x_forwarded_proto {
  default $http_x_forwarded_proto;
  ''      $scheme;
}
# If we receive X-Forwarded-Port, pass it through; otherwise, pass along the
# server port the client connected to
map $http_x_forwarded_port $proxy_x_forwarded_port {
  default $http_x_forwarded_port;
  ''      $server_port;
}
# If we receive Upgrade, set Connection to "upgrade"; otherwise, delete any
# Connection header that may have been passed to this server
map $http_upgrade $proxy_connection {
  default upgrade;
  '' close;
}
# Apply fix for very long server names
server_names_hash_bucket_size 128;
# Default dhparam
ssl_dhparam /etc/nginx/dhparam/dhparam.pem;
# Set appropriate X-Forwarded-Ssl header based on $proxy_x_forwarded_proto
map $proxy_x_forwarded_proto $proxy_x_forwarded_ssl {
  default off;
  https on;
}
gzip_types text/plain text/css application/javascript application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;
log_format vhost '$host $remote_addr - $remote_user [$time_local] '
                 '"$request" $status $body_bytes_sent '
                 '"$http_referer" "$http_user_agent" '
                 '"$upstream_addr"';
access_log off;
                ssl_protocols TLSv1.2 TLSv1.3;
                ssl_ciphers 'ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384';
                ssl_prefer_server_ciphers off;
error_log /dev/stderr;
resolver 8.8.8.8;
# HTTP 1.1 support
proxy_http_version 1.1;
proxy_buffering off;
proxy_set_header Host $http_host;
proxy_set_header Upgrade $http_upgrade;
proxy_set_header Connection $proxy_connection;
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header X-Forwarded-Proto $proxy_x_forwarded_proto;
proxy_set_header X-Forwarded-Ssl $proxy_x_forwarded_ssl;
proxy_set_header X-Forwarded-Port $proxy_x_forwarded_port;
proxy_set_header X-Original-URI $request_uri;
# Mitigate httpoxy attack (see README for details)
proxy_set_header Proxy "";
server {
        server_name _; # This is just an invalid value which will never trigger on a real hostname.
        server_tokens off;
        listen 80;
        access_log /var/log/nginx/access.log vhost;
        return 503;
}
        # proxy.example
upstream proxy.example {
        ## Can be connected with "bridge" network
        # compassionate_feynman
        server 172.18.0.4:80;
        ## Can be connected with "bridge" network
        # musing_meitner
        server 172.18.0.3:80;
}
server {
        server_name proxy.example;
        listen 80 default_server;
        access_log /var/log/nginx/access.log vhost;
        location / {
                proxy_pass http://proxy.example;
}
}
$ docker logs nginx
Info: running nginx-proxy version 1.0.1-6-gc4ad18f
Setting up DH Parameters..
forego      | starting dockergen.1 on port 5000
forego      | starting nginx.1 on port 5100
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: using the "epoll" event method
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: nginx/1.21.6
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: built by gcc 10.2.1 20210110 (Debian 10.2.1-6) 
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: OS: Linux 4.14.29-1-lts
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: getrlimit(RLIMIT_NOFILE): 1048576:1048576
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: start worker processes
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: start worker process 21
dockergen.1 | 2022/05/19 05:11:05 Generated '/etc/nginx/conf.d/default.conf' from 1 containers
dockergen.1 | 2022/05/19 05:11:05 Running 'nginx -s reload'
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: signal 1 (SIGHUP) received from 24, reconfiguring
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: reconfiguring
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: using the "epoll" event method
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: start worker processes
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: start worker process 25
dockergen.1 | 2022/05/19 05:11:05 Watching docker events
dockergen.1 | 2022/05/19 05:11:05 Contents of /etc/nginx/conf.d/default.conf did not change. Skipping notification 'nginx -s reload'
nginx.1     | 2022/05/19 05:11:05 [notice] 21#21: gracefully shutting down
nginx.1     | 2022/05/19 05:11:05 [notice] 21#21: exiting
nginx.1     | 2022/05/19 05:11:05 [notice] 21#21: exit
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: signal 17 (SIGCHLD) received from 21
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: worker process 21 exited with code 0
nginx.1     | 2022/05/19 05:11:05 [notice] 17#17: signal 29 (SIGIO) received
nginx.1     | docker 10.0.0.28 - - [19/May/2022:05:11:09 +0000] "GET / HTTP/1.1" 503 190 "-" "curl/7.47.0" "-"
dockergen.1 | 2022/05/19 05:11:22 Received event start for container 4635af625132
dockergen.1 | 2022/05/19 05:11:22 Template error: open /etc/nginx/certs: no such file or directory
dockergen.1 | 2022/05/19 05:11:22 Generated '/etc/nginx/conf.d/default.conf' from 2 containers
dockergen.1 | 2022/05/19 05:11:22 Running 'nginx -s reload'
nginx.1     | 2022/05/19 05:11:22 [notice] 17#17: signal 1 (SIGHUP) received from 31, reconfiguring
nginx.1     | 2022/05/19 05:11:22 [notice] 17#17: reconfiguring
nginx.1     | 2022/05/19 05:11:22 [notice] 17#17: using the "epoll" event method
nginx.1     | 2022/05/19 05:11:22 [notice] 17#17: start worker processes
nginx.1     | 2022/05/19 05:11:22 [notice] 17#17: start worker process 32
nginx.1     | 2022/05/19 05:11:22 [notice] 25#25: gracefully shutting down
nginx.1     | 2022/05/19 05:11:22 [notice] 25#25: exiting
nginx.1     | 2022/05/19 05:11:22 [notice] 25#25: exit
nginx.1     | 2022/05/19 05:11:22 [notice] 17#17: signal 17 (SIGCHLD) received from 25
nginx.1     | 2022/05/19 05:11:22 [notice] 17#17: worker process 25 exited with code 0
nginx.1     | 2022/05/19 05:11:22 [notice] 17#17: signal 29 (SIGIO) received
nginx.1     | docker 10.0.0.28 - - [19/May/2022:05:11:30 +0000] "GET / HTTP/1.1" 200 58 "-" "curl/7.47.0" "172.18.0.3:80"
dockergen.1 | 2022/05/19 05:11:44 Received event start for container aa5e40dae692
dockergen.1 | 2022/05/19 05:11:44 Template error: open /etc/nginx/certs: no such file or directory
dockergen.1 | 2022/05/19 05:11:44 Generated '/etc/nginx/conf.d/default.conf' from 3 containers
dockergen.1 | 2022/05/19 05:11:44 Running 'nginx -s reload'
nginx.1     | 2022/05/19 05:11:45 [notice] 17#17: signal 1 (SIGHUP) received from 34, reconfiguring
nginx.1     | 2022/05/19 05:11:45 [notice] 17#17: reconfiguring
nginx.1     | 2022/05/19 05:11:45 [notice] 17#17: using the "epoll" event method
nginx.1     | 2022/05/19 05:11:45 [notice] 17#17: start worker processes
nginx.1     | 2022/05/19 05:11:45 [notice] 17#17: start worker process 35
nginx.1     | 2022/05/19 05:11:45 [notice] 32#32: gracefully shutting down
nginx.1     | 2022/05/19 05:11:45 [notice] 32#32: exiting
nginx.1     | 2022/05/19 05:11:45 [notice] 32#32: exit
nginx.1     | 2022/05/19 05:11:45 [notice] 17#17: signal 17 (SIGCHLD) received from 32
nginx.1     | 2022/05/19 05:11:45 [notice] 17#17: worker process 32 exited with code 0
nginx.1     | 2022/05/19 05:11:45 [notice] 17#17: signal 29 (SIGIO) received
nginx.1     | docker 10.0.0.28 - - [19/May/2022:05:11:46 +0000] "GET / HTTP/1.1" 200 58 "-" "curl/7.47.0" "172.18.0.4:80"
$
```