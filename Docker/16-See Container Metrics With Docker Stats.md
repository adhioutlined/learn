When running containers in production, it's important to monitor there runtime metrics, such as CPU usage and memory, to ensure they're behaving as expected. These metrics can also help diagnose issues if they occur.

In this scenario, we'll explore the built-in metrics provided by Docker to give additional visibility to the running containers. In future, we'll investigate more details about running Docker in production.

#### Step 1 - Single Container

The environment has a container running with the name  _nginx_. You can find the stats for the container by using:

`docker stats nginx`

This launches a terminal window which refreshes itself with live data from the container.

To quit, use CTRL+C to stop the running progress.

#### Step 2 - Multiple Containers

The built-in Docker allows you to provide multiple names/ids and display their stats within a single window.

The environment now has three connected containers running. To view the stats for all these containers you can use pipes and xargs. A pipe passes the output from one command into the input of another while xargs allows you to provide this input as arguments to a command.

By combining the two we can take the list of all our running containers provided by  `docker ps`  and use them as the argument for  `docker stats`. This gives us an overview of the entire machine's containers.

`docker ps -q | xargs docker stats`

To quit, use CTRL+C to stop the running progress.



```
$ docker run -d -p 80:80 -e DEFAULT_HOST=proxy.example -v /var/run/docker.sock:/tmp/docker.sock:ro --name nginx jwilder/nginx-proxy:alpine

```
