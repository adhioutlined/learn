In this scenario you will learn how to use the _--format__ parameters to pretty-print output from `docker ps` and `docker inspect`.

#### Example 1 - Names and Images as Table

The format of  `docker ps`  can be formatted to only display the information relevant to you.

## Start Example Container

Start by launching a example container -  `docker run -d redis`

## Format

The standard  `docker ps`  command outputs the name, image used, command, uptime and port information.

To limit which columns are displayed, use the _--format__ parameter. The parameter allows pretty-printing containers using a Go template syntax.

`docker ps --format '{{.Names}} container is using {{.Image}} image'`

As it's using Go templates, it includes helper functions such as  _table_.

`docker ps --format 'table {{.Names}}\t{{.Image}}'`

#### Example 2 - List IP addresses

However, the format parameter allow supports displaying data that is already exposed via the  `docker ps`  command. If you wanted to include additional information, such as the IP Address of the container, then the data needs to come via  `docker inspect`.

Thankfully, the  `docker inspect`  also supports pretty-printing the results via a Go Template. The container IDs from  `docker ps`  can be piped into  `docker inspect`.

The format parameter can then access all of the container information. Below is an example of listing all the IP addresses for the running containers.

`docker ps -q | xargs docker inspect --format '{{ .Id }} - {{ .Name }} - {{ .NetworkSettings.IPAddress }}'`

#### Complete Command
```
Your Interactive Bash Terminal.
A good starting point is executing `docker`

$ docker run -d redis
3b1bbf484ed63df1840faf8addae4039ac0e472c12aa502b4114d5ad21d49fc0
$ ls
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
3b1bbf484ed6        redis               "docker-entrypoint.s…"   8 seconds ago       Up 7 seconds        6379/tcp            admiring_hopper
$ docker ps --format '{{.Names}} container is using {{.Image}} image'
admiring_hopper container is using redis image
$ docker ps --format 'table {{.Names}}\t{{.Image}}'
NAMES               IMAGE
admiring_hopper     redis
$ docker ps -q | xargs docker inspect --format '{{ .Id }} - {{ .Name }} - {{ .NetworkSettings.IPAddress }}'
3b1bbf484ed63df1840faf8addae4039ac0e472c12aa502b4114d5ad21d49fc0 - /admiring_hopper - 172.18.0.2
$ 
```